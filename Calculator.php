<!DOCTYPE html>

<html>
<head>
    <title>Simple Calculator</title>
</head>

<body>
<form method="get">  
<input type="text" name="first" id="first"/>
<input type="radio" name="op" id="radio" value="add">+</input>
<input type="radio" name="op" id="radio" value="sub">&#8722</input>
<input type="radio" name="op" id="radio" value="mult">&times</input>
<input type="radio" name="op" id="radio" value="div">&divide</input>
<input type="text" name="second" id="second"/>
<input type="submit"  value="="/>
<?php
    if (isset($_GET['first']) && isset($_GET['second'])) {
        if (is_numeric($_GET['first']) && is_numeric($_GET['second'])){
            $first = (float) $_GET['first'];
            $second = (float) $_GET['second'];
            if (isset($_GET['op'])){
                $op = $_GET['op'];
                switch($op){
                case "add":
                    $output = $first." + ".$second." = ".($first+$second);
                    break;
                case "sub":
                    $output = $first." - ".$second." = ".($first-$second);
                    break;
                case "mult":
                    $output = $first." &times ".$second." = ".($first*$second);
                    break;
                case "div":
                    if ($second != 0){
                        $output = $first." &divide ".$second." = ".($first/$second);
                    }else{
                        $output = "Cannot divide by zero!";
                    }
                    break;
                default:
                    break;
                }
            }else{
                $output = "Please select an operator.";
            }
        }else{
            $output = "Please provide numeric inputs.";
        } 
    }else{
        $output = "Please provide two inputs.";
    }
    printf("%s", htmlentities($output));
?>
</form> 


</body>
</html>
